import 'package:flutter/material.dart';
import 'package:rick_and_morty_clean_arch/feature/presentation/widgets/persons_list.dart';

class PersonScreen extends StatelessWidget {
  const PersonScreen({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Characters',
          style: TextStyle(color: Colors.white),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.search),
            color: Colors.white,
          )
        ],
      ),
      body: PersonsList(),
    );
  }
}
