part of 'search_bloc.dart';

sealed class SearchState extends Equatable {
  const SearchState();

  @override
  List<Object> get props => [];
}

final class SearchInitial extends SearchState {}

final class SearchLoading extends SearchState {}

final class SearchSuccess extends SearchState {
  final List<PersonEntity> persons;

  const SearchSuccess({required this.persons});

  @override
  List<Object> get props => [persons];
}

final class SearchError extends SearchState {
  final String message;

  const SearchError({required this.message});
}
