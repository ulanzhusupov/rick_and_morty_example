import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:rick_and_morty_clean_arch/core/platform/network_info.dart';
import 'package:rick_and_morty_clean_arch/feature/data/datasources/local_datasource/person_local_datasource.dart';
import 'package:rick_and_morty_clean_arch/feature/data/datasources/remote_datasource/person_remote_datasource.dart';
import 'package:rick_and_morty_clean_arch/feature/data/repositories/person_repository_impl.dart';
import 'package:rick_and_morty_clean_arch/feature/domain/repositories/person_repository.dart';
import 'package:rick_and_morty_clean_arch/feature/domain/usecases/get_all_persons.dart';
import 'package:rick_and_morty_clean_arch/feature/presentation/bloc/person_list_cubit/person_list_cubit.dart';
import 'package:rick_and_morty_clean_arch/feature/presentation/bloc/search_bloc/search_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

final sl = GetIt.instance;

Future<void> init() async {
  //Bloc
  sl.registerFactory(() => PersonListCubit(getAllPersons: sl()));
  sl.registerFactory(() => PersonSearchBloc(searchPerson: sl()));

  //USeCAses
  sl.registerLazySingleton(() => GetAllPersons(personRepository: sl()));
  sl.registerLazySingleton(() => SearchPersons(personQuery: sl()));

  //Repository
  sl.registerLazySingleton<PersonRepository>(() => PersonrepositoryImpl(
        remoteDataSource: sl(),
        localDataSource: sl(),
        networkInfo: sl(),
      ));

  sl.registerLazySingleton<PersonRemoteDataSource>(
    () => PersonRemoteDataSourceImpl(
      client: http.Client(),
    ),
  );

  sl.registerLazySingleton<PersonLocalDataSource>(
      () => PersonLocalDataSourceImpl(sharedPreferences: sl()));

  //Core
  sl.registerLazySingleton<NetworkInfo>(
      () => NetworkInfoImpl(connectionChecker: sl()));

  //External
  final sharedPref = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPref);
  sl.registerLazySingleton(() => http.Client());
  sl.registerLazySingleton(() => InternetConnectionChecker());
}
