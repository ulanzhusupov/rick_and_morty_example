import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:rick_and_morty_clean_arch/core/consts/app_consts.dart';
import 'package:rick_and_morty_clean_arch/core/error/failure.dart';
import 'package:rick_and_morty_clean_arch/feature/domain/entities/person_entity.dart';
import 'package:rick_and_morty_clean_arch/feature/domain/usecases/get_all_persons.dart';

part 'person_list_state.dart';

class PersonListCubit extends Cubit<PersonListState> {
  final GetAllPersons getAllPersons;

  PersonListCubit({required this.getAllPersons}) : super(PersonListInitial());

  int page = 1;

  void loadPerson() async {
    if (state is PersonListLoading) return;

    final currentState = state;

    var oldPersons = <PersonEntity>[];
    if (currentState is PersonListSuccess) {
      oldPersons = currentState.personsList;
    }

    emit(PersonListLoading(oldPersons, isFirstFetch: page == 1));

    final failureOrPerson = await getAllPersons(PagePersonParams(page: page));
    page++;

    failureOrPerson.fold(
        (error) => emit(PersonListError(message: _mapFailureToMessage(error))),
        (person) {
      final persons = (state as PersonListLoading).oldPersonsList;
      persons.addAll(person);
      emit(PersonListSuccess(personsList: persons));
    });
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return AppConsts.SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return AppConsts.CACHE_FAILURE_MESSAGE;
      default:
        return 'Unexpected Error';
    }
  }
}
