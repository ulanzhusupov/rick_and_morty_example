import 'dart:convert';

import 'package:rick_and_morty_clean_arch/core/consts/app_consts.dart';
import 'package:rick_and_morty_clean_arch/core/error/exception.dart';
import 'package:rick_and_morty_clean_arch/feature/data/models/person_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class PersonLocalDataSource {
  Future<List<PersonModel>> getLastPersonsFromCache();
  Future<void> personsToCache(List<PersonModel> persons);
}

class PersonLocalDataSourceImpl implements PersonLocalDataSource {
  final SharedPreferences sharedPreferences;

  PersonLocalDataSourceImpl({required this.sharedPreferences});

  @override
  Future<List<PersonModel>> getLastPersonsFromCache() {
    final jsonPersonList =
        sharedPreferences.getStringList(AppConsts.CACHED_PERSONS_LIST);
    if (jsonPersonList?.isNotEmpty ?? false) {
      return Future.value(jsonPersonList
          ?.map((person) => PersonModel.fromJson(jsonDecode(person)))
          .toList());
    } else {
      throw CacheException();
    }
  }

  @override
  Future<void> personsToCache(List<PersonModel> persons) {
    final List<String> jsonPersonsList =
        persons.map((person) => jsonEncode(person.toJson())).toList();

    sharedPreferences.setStringList(
        AppConsts.CACHED_PERSONS_LIST, jsonPersonsList);
    print("Writin persons to cache");
    return Future.value(jsonPersonsList);
  }
}
