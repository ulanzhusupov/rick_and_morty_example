import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty_clean_arch/feature/domain/entities/person_entity.dart';
import 'package:rick_and_morty_clean_arch/feature/presentation/bloc/person_list_cubit/person_list_cubit.dart';
import 'package:rick_and_morty_clean_arch/feature/presentation/widgets/person_card.dart';

class PersonsList extends StatelessWidget {
  PersonsList({super.key});
  final scrollController = ScrollController();

  void setupScrollController(BuildContext context) {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels != 0) {
          context.read<PersonListCubit>().loadPerson();
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    setupScrollController(context);

    return BlocBuilder<PersonListCubit, PersonListState>(
      builder: (context, state) {
        List<PersonEntity> persons = [];
        bool isLoading = false;

        if (state is PersonListLoading && state.isFirstFetch) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is PersonListLoading) {
          persons = state.oldPersonsList;
          isLoading = true;
        }

        if (state is PersonListError) {
          return Center(
            child: Text(
              state.message,
              style: const TextStyle(color: Colors.white),
            ),
          );
        }
        if (state is PersonListSuccess) {
          persons = state.personsList;
        }

        return ListView.separated(
          controller: scrollController,
          itemBuilder: (context, index) {
            if (index < persons.length) {
              return PersonCard(person: persons[index]);
            } else {
              Timer(const Duration(milliseconds: 100), () {
                scrollController
                    .jumpTo(scrollController.position.maxScrollExtent);
              });
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          },
          separatorBuilder: (context, index) {
            return const Divider(
              color: Colors.lightGreen,
            );
          },
          itemCount: persons.length + (isLoading ? 1 : 0),
        );
      },
    );
  }
}
