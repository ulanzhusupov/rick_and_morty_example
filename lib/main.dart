import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty_clean_arch/feature/presentation/bloc/person_list_cubit/person_list_cubit.dart';
import 'package:rick_and_morty_clean_arch/feature/presentation/bloc/search_bloc/search_bloc.dart';
import 'package:rick_and_morty_clean_arch/feature/presentation/pages/person_screen.dart';
import 'package:rick_and_morty_clean_arch/feature/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_clean_arch/locator_service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<PersonListCubit>(
            create: (context) => sl<PersonListCubit>()..loadPerson()),
        BlocProvider<PersonSearchBloc>(
            create: (context) => sl<PersonSearchBloc>()),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
          scaffoldBackgroundColor: AppColors.mainBg,
          appBarTheme: const AppBarTheme(
            backgroundColor: AppColors.mainBg,
          ),
        ),
        home: const PersonScreen(),
      ),
    );
  }
}
