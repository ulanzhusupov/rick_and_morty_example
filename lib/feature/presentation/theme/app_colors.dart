import 'package:flutter/material.dart';

abstract class AppColors {
  static const Color mainBg = Color(0xff24282f);
  static const Color cellBg = Color(0xff3c3e44);
  static const Color grey = Color(0xff9e9e9e);
}
