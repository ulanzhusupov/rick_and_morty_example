part of 'person_list_cubit.dart';

sealed class PersonListState extends Equatable {
  const PersonListState();

  @override
  List<Object> get props => [];
}

final class PersonListInitial extends PersonListState {}

class PersonListLoading extends PersonListState {
  final List<PersonEntity> oldPersonsList;
  final bool isFirstFetch;

  const PersonListLoading(
    this.oldPersonsList, {
    this.isFirstFetch = false,
  });

  @override
  List<Object> get props => [oldPersonsList];
}

class PersonListSuccess extends PersonListState {
  final List<PersonEntity> personsList;

  const PersonListSuccess({required this.personsList});

  @override
  List<Object> get props => [personsList];
}

class PersonListError extends PersonListState {
  final String message;

  const PersonListError({required this.message});

  @override
  List<Object> get props => [message];
}
