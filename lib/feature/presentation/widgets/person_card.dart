import 'package:flutter/material.dart';
import 'package:rick_and_morty_clean_arch/feature/domain/entities/person_entity.dart';
import 'package:rick_and_morty_clean_arch/feature/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_clean_arch/feature/presentation/widgets/person_cache_image.dart';

class PersonCard extends StatelessWidget {
  const PersonCard({super.key, required this.person});

  final PersonEntity person;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.cellBg,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Row(
        children: [
          SizedBox(
            child: PersonCacheImage(
                imageUrl: person.image ?? "", width: 166, height: 166),
          ),
          const SizedBox(width: 16),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 12),
                Text(
                  "${person.name}",
                  style: const TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(height: 4),
                Row(
                  children: [
                    Container(
                      width: 8,
                      height: 8,
                      decoration: BoxDecoration(
                        color: person.status?.toLowerCase() == 'alive'
                            ? Colors.green
                            : Colors.red,
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                    const SizedBox(width: 8),
                    Expanded(
                      child: Text(
                        "${person.status} - ${person.species}",
                        style: const TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 12),
                const Text(
                  "Last Known Location:",
                  style: TextStyle(color: AppColors.grey),
                ),
                const SizedBox(height: 2),
                Text(
                  person.location?.name ?? "",
                  style: const TextStyle(
                    color: Colors.white,
                  ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                const SizedBox(height: 6),
                const Text(
                  "Origin:",
                  style: TextStyle(color: AppColors.grey),
                ),
                const SizedBox(height: 2),
                Text(
                  person.origin?.name ?? "",
                  style: const TextStyle(
                    color: Colors.white,
                  ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                const SizedBox(height: 16),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
