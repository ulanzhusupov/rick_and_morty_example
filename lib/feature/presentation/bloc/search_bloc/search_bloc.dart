import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:rick_and_morty_clean_arch/core/consts/app_consts.dart';
import 'package:rick_and_morty_clean_arch/core/error/failure.dart';
import 'package:rick_and_morty_clean_arch/feature/domain/entities/person_entity.dart';
import 'package:rick_and_morty_clean_arch/feature/domain/usecases/search_person.dart';

part 'search_event.dart';
part 'search_state.dart';

class PersonSearchBloc extends Bloc<PersonSearchEvent, SearchState> {
  final SearchPerson searchPerson;

  PersonSearchBloc({required this.searchPerson}) : super(SearchInitial()) {
    on<PersonSearchEvent>((event, emit) async {
      if (event is SearchPersons) {
        emit(SearchLoading());
        final failureOrPerson =
            await searchPerson(SearchPersonParams(query: event.personQuery));

        failureOrPerson.fold(
          (error) => emit(SearchError(message: _mapFailureToMessage(error))),
          (persons) => emit(SearchSuccess(persons: persons)),
        );
      }
    });
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return AppConsts.SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return AppConsts.CACHE_FAILURE_MESSAGE;
      default:
        return 'Unexpected Error';
    }
  }
}
