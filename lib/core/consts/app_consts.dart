abstract class AppConsts {
  static const String CACHED_PERSONS_LIST = "CACHED_PERSONS_LIST";
  static const String SERVER_FAILURE_MESSAGE = "SERVER_FAILURE_MESSAGE";
  static const String CACHE_FAILURE_MESSAGE = "CACHE_FAILURE_MESSAGE";
}
